/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.mycompany.ejerciciocero;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Jonathan
 */
public class EjercicioClase1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int[] jerarquia= {0,0,1,3,4,1,2,2,7,6};
        String[] nombres= {"Carlos","Juan","Maria","Carlos1","Sofia","Hernán","Wilson","Diana","Mateo","Efrain"};
        String[] arbol= new String[nombres.length];
        ArrayList<Integer> numerosUnicos = new ArrayList<>();
        
        for(int i=0; i<jerarquia.length;i++){           
            if( !numerosUnicos.contains(jerarquia[i])){
                numerosUnicos.add(jerarquia[i]);
                arbol[jerarquia[i]]= nombres[i];
                
            }else{              
                arbol[jerarquia[i]] = arbol[jerarquia[i]]+" "+nombres[i];
            }

        }
        
        for(int i=0; i<nombres.length;i++){
            System.out.println(arbol[i]);
        }
        
        System.out.println(arbol[0].substring(0,arbol[0].indexOf(" "))+"\t\t\t"+arbol[0].substring(arbol[0].indexOf(" ")+1));
        for(int i=1; i<arbol.length;i+=2){
            String hijos=arbol[i]+arbol[i+1];
            
            if(arbol[i].contains(" ")){
                System.out.println(arbol[i].substring(0,arbol[i].indexOf(" "))+"\t\t\t"+arbol[i].substring(arbol[i].indexOf(" ")+1));
            }else{
                
            }
            
        }
        
    }
    
}
